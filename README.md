PRTG Device Template for Brocade FibreChannel devices
===========================================

Please document and rename this file to "README.md"

This project contains all the files necessary to integrate the Brocade FibreChannel
into PRTG for auto discovery and sensor creation.

 The template creates sensors for:
  - System Status: CPU %, Mem%, System Op/Admin Status, Flash Op/Admin Status, Beacon Op Status, Diag results(Last), Sensors(Cnt)
  - Hardware Sensors (Status[Temp, Voltage, Current, etc], type, value)
  - Port Status
          Op Status
          Phy State
          Tx/Rx Frames
          Rx C3 Frames
          LCS Frames
          No Tx Cred
          Rx CRC Err
          Rx Bad EOF, X Bad Enc/Outs Frame
  

Download Instructions
=========================
 A zip file containing all the files in the project can be [downloaded from the 
repository](https://gitlab.com/PRTG/Device-Templates/Brocade-FC/-/jobs/artifacts/master/download?job=PRTGDistZip) 


Installation Instructions
=========================
Please refer to INSTALL.md
